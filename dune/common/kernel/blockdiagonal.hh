// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_COMMON_KERNEL_BLOCKDIAGONAL_HH
#define DUNE_COMMON_KERNEL_BLOCKDIAGONAL_HH

#include <dune/common/kernel/blockdiagonal/extractbelldiagonal.hh>
#include <dune/common/kernel/blockdiagonal/ludecompositionnopivot.hh>
#include <dune/common/kernel/blockdiagonal/ludecompositionpartialpivot.hh>
#include <dune/common/kernel/blockdiagonal/lusolvenopivot.hh>
#include <dune/common/kernel/blockdiagonal/lusolvepartialpivot.hh>

#endif // DUNE_COMMON_KERNEL_BLOCKDIAGONAL_HH
