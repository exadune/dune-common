// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_COMMON_KERNEL_ELL_HH
#define DUNE_COMMON_KERNEL_ELL_HH

#include <dune/common/kernel/ell/mmv.hh>
#include <dune/common/kernel/ell/mv.hh>
#include <dune/common/kernel/ell/preconditioners/jacobi.hh>
#include <dune/common/kernel/ell/umv.hh>
#include <dune/common/kernel/ell/usmv.hh>

#endif // DUNE_COMMON_KERNEL_ELL_HH
