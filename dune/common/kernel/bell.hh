// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_COMMON_KERNEL_BELL_HH
#define DUNE_COMMON_KERNEL_BELL_HH

#include <dune/common/kernel/bell/mmv.hh>
#include <dune/common/kernel/bell/mv.hh>
#include <dune/common/kernel/bell/preconditioners/jacobinolupivot.hh>
#include <dune/common/kernel/bell/preconditioners/sornolupivot.hh>
#include <dune/common/kernel/bell/umv.hh>
#include <dune/common/kernel/bell/usmv.hh>

#endif // DUNE_COMMON_KERNEL_BELL_HH
