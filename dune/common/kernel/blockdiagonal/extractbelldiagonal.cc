// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#include "config.h"

#define DUNE_KERNEL_GENERATE_DEFINITIONS 1

#include <dune/common/kernel/blockdiagonal/extractbelldiagonal.hh>
